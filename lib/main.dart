import 'package:flutter/material.dart';

void main() => runApp(HelloFlutterApp());

class HelloFlutterApp extends StatefulWidget {
  @override
  _HelloFlutterAppState createState() => _HelloFlutterAppState();
}
String english = "Hello Flutter";
String spain = "Hola Flutter";
String japan = "Kon'nichiwa Flutter";
String korea = "annyeonghaseyo Flutter";

class _HelloFlutterAppState extends State<HelloFlutterApp> {
  String displayText =english;
  @override
  Widget build(BuildContext context){
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text("Hello flutter"),
          leading: Icon(Icons.home),
          actions: <Widget> [
            IconButton(
                onPressed: () {
                  setState(() {
                    displayText = displayText == english?
                        spain:english;

                  });
                },
                icon: Icon(Icons.translate)),
            IconButton(
                onPressed: () {
                  setState(() {

                    displayText = displayText == english?
                    japan:english;

                  });
                },
                icon: Icon(Icons.yard)),
            IconButton(
                onPressed: () {
                  setState(() {
                    displayText = displayText == english?
                    korea:english;
                  });
                },
                icon: Icon(Icons.refresh))
          ],
        ),

        body: Center(
          child: Text(
            displayText,
            style: TextStyle(fontSize: 24),
          ),
        ),
      ),

    );
  }
}


